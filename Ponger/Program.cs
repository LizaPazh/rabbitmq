﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.Text;
using System.Threading;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();

            var settings = config.GetSection("RabbitSettings").Get<RabbitSettings>();

            using (Service wrapper = new Service(settings))
            {
                Console.WriteLine("ponger started! (ctr+c to exit)");
                wrapper.ListenQueue();
                Console.ReadLine();
            }
        }
    }
}
