﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class Service : IDisposable
    {
        private IConnection connection;
        private IModel channel;

        private readonly string _publishQueue;
        private readonly string _listenQueue;
        private readonly string _exchangeType;
        private readonly string _exchange;
        private readonly string _responseMessage;
        private readonly int _responseTimeout;

        private void InitRabbitMQ()
        {
            var factory = new ConnectionFactory { HostName = "localhost" };

            connection = factory.CreateConnection();

            channel = connection.CreateModel();
        }

        public Service(RabbitSettings settings)
        {
            InitRabbitMQ();
            _publishQueue = settings.PublishQueue;
            _listenQueue = settings.ListenQueue;
            _exchangeType = settings.ExchangeType;
            _exchange = settings.Exchange;
            _responseMessage = settings.ResponseMessage;
            _responseTimeout = settings.ResponseTimeout;
        }

        public void SendMessageToQueue(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            channel.ExchangeDeclare(_exchange, _exchangeType);

            channel.BasicPublish(exchange: _exchange,
                                    routingKey: _publishQueue,
                                    basicProperties: null,
                                    body: body);
            Console.WriteLine(" [x] Sent {0} at {1}", message, DateTime.Now);
        }

        public void ListenQueue()
        {
            channel.ExchangeDeclare(_exchange, _exchangeType);
            channel.QueueDeclare(queue: _listenQueue,
                                    durable: false,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);
            channel.QueueBind(_listenQueue, exchange: _exchange, routingKey: _listenQueue);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += MessageReceived;

            channel.BasicConsume(queue: _listenQueue,
                                 autoAck: false,
                                 consumer: consumer);
        }
        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            Console.WriteLine(" [x] Received {0} at {1}", message, DateTime.Now);
            channel.BasicAck(args.DeliveryTag, multiple: false);
            Thread.Sleep(_responseTimeout);
            SendResponse();
        }
        private void SendResponse()
        {
            SendMessageToQueue(_responseMessage);
        }

        public void Dispose()
        {
            channel?.Dispose();
            connection?.Dispose();
        }
    }
}
