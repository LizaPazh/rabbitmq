﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public class RabbitSettings
    {
        public string ListenQueue { get; set; }
        public string PublishQueue { get; set; }
        public string ExchangeType { get; set; }
        public string Exchange{ get; set; }
        public string ResponseMessage { get; set; }
        public int ResponseTimeout { get; set; }
    }
}
